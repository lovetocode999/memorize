var memorizing = false;
var hiddenWords = [undefined];
var verse;
var verseArray;
var memorizeDiv = document.getElementById('memorize');
var check = document.getElementById('check');
function memorize() {
  if(memorizing) {
    memorizing = false;
    hiddenWords = [undefined];
    document.getElementsByTagName('button')[0].innerHTML = "Memorize";
    memorizeDiv.setAttribute('contenteditable', true);
    memorizeDiv.innerText = verse;
    check.setAttribute('contenteditable', false);
    check.innerText = '';
  } else {
    memorizing = true;
    verse = memorizeDiv.innerText;
    verseArray = memorizeDiv.innerText.split(' ');
    document.getElementsByTagName('button')[0].innerText = "Stop Memorizing";
    memorizeDiv.setAttribute('contenteditable', false);
    check.setAttribute('contenteditable', true);
  }
}
check.addEventListener('input', function(){
  updateDisplay();
}, false);
function updateDisplay() {
  if(verse.length > check.innerText.length) {
    if(verse.substring(0, check.innerText.length).trim() == check.innerText.trim()) {
      check.style.color = 'black';
    } else {
      check.style.color = 'red';
    }
  } else if(verse.length < check.innerText.length) {
    check.style.color = 'red';
  } else if(verse.length == check.innerText.length) {
    if(verse.trim() == check.innerText.trim()) {
      check.style.color = 'black';
      resetCheck();
      hideRandomWord();
    } else {
      check.style.color = 'red';
    }
  }
}
function resetCheck() {
  check.innerHTML = '';
}
function hideRandomWord() {
  var b;
  var span;
  memorizeDiv.innerHTML = '';
  if(hiddenWords.length-1 !== verseArray.length) {
    var randomInt;
    while(hiddenWords.includes(randomInt)) {
      randomInt = getRandomInt(verseArray.length);
    }
    hiddenWords.push(randomInt);
  }
  for(i = 0; i < verseArray.length; i++) {
    if(hiddenWords.includes(i)) {
      b = document.createElement('b');
      b.innerText = verseArray[i]+' ';
      memorizeDiv.appendChild(b);
    } else {
      span = document.createElement('span');
      span.innerText = verseArray[i]+' '
      memorizeDiv.appendChild(span);
    }
  }
}
function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}
